import {OrderRow} from './order-row';
import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';

@inject(DialogController)
export class AddItem {     
  heading = 'Add an Item to the Order...';
 
  constructor(controller) {
    this.controller = controller;
    this.itemDescription = '';
    this.itemOrderedBy = '';
    this.itemAmount = '';
  }

  activate(orders) {
    //load the items to dropdown
    this.orders = orders;
  }

  addItem() {
    if (this.itemDescription && this.itemOrderedBy && this.itemAmount) {
      this.orders.orderItems.push(new OrderRow(this.itemDescription, this.itemOrderedBy, this.itemAmount));
      this.orders.itemsTotal = this.orders.itemsTotal + parseFloat(this.itemAmount);
      this.controller.ok();
    }
  }
  


 }


