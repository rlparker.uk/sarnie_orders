
export class OrderRow {
      constructor(description,itemOrderedBy,itemAmount) {
        this.description = description;
        this.itemOrderedBy = itemOrderedBy;
        this.itemAmount = itemAmount;
      }

    }
