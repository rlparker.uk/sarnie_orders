import {HttpClient} from 'aurelia-http-client';  
 
export class MenuService {  
  constructor(){
      this.http = new HttpClient();
  }

  getMenuItems(){
      return this.http.get('http://s44b1808/cgi-bin/jsmdirect?HaydonsMenuOutboundService');
  }
}
