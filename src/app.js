
    import {inject} from 'aurelia-framework';
    import {DialogService} from 'aurelia-dialog';
    import {AddItem} from './add-item';

    @inject(DialogService)
    export class App {

      constructor(dialogService) {
        this.dialogService = dialogService;
        this.heading = "Haydons";
        this.orders = { orderItems: [], itemsTotal: 0};
      }

      rate() {
        this.dialogService.open({viewModel: AddItem, model:this.orders })
      }


      

      removeItem(item) {
        let index = this.orders.orderItems.indexOf(item);
        if (index !== -1) {
          this.orders.orderItems.splice(index, 1);
          this.orders.itemsTotal = this.orders.itemsTotal - parseFloat(item.itemAmount);
        }
      }
    }
